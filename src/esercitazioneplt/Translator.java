package esercitazioneplt;

public class Translator {
	public static final String NIL = null;
	private String phrase;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translateBaseSpace(String imputPhraseBase) {

		if (imputPhraseBase.contains(" ") && !imputPhraseBase.contains("-")) {

			String[] parts = imputPhraseBase.split(" ");

			String carattereSpeciale = "";

			String ospitePhrase = "";

			if (endWithPunctuations(parts[0])) {

				Translator translator1 = new Translator(parts[0].substring(0, parts[0].length() - 1));
				String part3 = translator1.translate();
				ospitePhrase = part3;

				carattereSpeciale = parts[0].substring(parts[0].length() - 1, parts[0].length());

				ospitePhrase = String.join("", ospitePhrase, carattereSpeciale);

			} else {

				Translator translator = new Translator(parts[0]);
				String part1 = translator.translate();

				ospitePhrase = part1;

			}

			for (int i = 1; i < parts.length; i++) {

				if (endWithPunctuations(parts[i])) {

					Translator translator1 = new Translator(parts[i].substring(0, parts[i].length() - 1));
					String part3 = translator1.translate();
					ospitePhrase = String.join(" ", ospitePhrase, part3);

					carattereSpeciale = parts[i].substring(parts[i].length() - 1, parts[i].length());

					ospitePhrase = String.join("", ospitePhrase, carattereSpeciale);

				} else {

					Translator translator1 = new Translator(parts[i]);
					String part2 = translator1.translate();

					ospitePhrase = String.join(" ", ospitePhrase, part2);

				}

			}

			return ospitePhrase;

		} else if (imputPhraseBase.contains("-") && !imputPhraseBase.contains(" ")) {

			String[] parts = imputPhraseBase.split("-");

			String carattereSpeciale1 = "";

			String ospitePhrase = "";

			if (endWithPunctuations(parts[0])) {

				Translator translator1 = new Translator(parts[0].substring(0, parts[0].length() - 1));
				String part3 = translator1.translate();
				ospitePhrase = part3;

				carattereSpeciale1 = parts[0].substring(parts[0].length() - 1, parts[0].length());

				ospitePhrase = String.join("", ospitePhrase, carattereSpeciale1);

			} else {

				Translator translator = new Translator(parts[0]);
				String part1 = translator.translate();

				ospitePhrase = part1;
			}

			for (int i = 1; i < parts.length; i++) {

				if (endWithPunctuations(parts[i])) {

					Translator translator1 = new Translator(parts[i].substring(0, parts[i].length() - 1));
					String part3 = translator1.translate();
					ospitePhrase = String.join("-", ospitePhrase, part3);

					carattereSpeciale1 = parts[i].substring(parts[i].length() - 1, parts[i].length());

					ospitePhrase = String.join("", ospitePhrase, carattereSpeciale1);

				} else {

					Translator translator1 = new Translator(parts[i]);
					String part2 = translator1.translate();

					ospitePhrase = String.join("-", ospitePhrase, part2);

				}

			}

			return ospitePhrase;

		} else if (imputPhraseBase.contains(" ") && imputPhraseBase.contains("-")) {

			String ospitePhrase = "";
			String carattereSpeciale2 = "";

			String[] parts = imputPhraseBase.split(" ");

			for (int i = 0; i < parts.length; i++) {

				if (!parts[i].contains("-")) {

					if (parts[i].equals(parts[0])) {

						if (endWithPunctuations(parts[0])) {

							Translator translator3 = new Translator(parts[i].substring(0, parts[0].length() - 1));
							String part3 = translator3.translate();
							ospitePhrase = part3;

							carattereSpeciale2 = parts[0].substring(parts[0].length() - 1, parts[0].length());

							ospitePhrase = String.join("", ospitePhrase, carattereSpeciale2);

						} else {

							Translator translator1 = new Translator(parts[0]);
							String part2 = translator1.translate();

							ospitePhrase = part2;

						}

					} else {

						if (endWithPunctuations(parts[i])) {

							Translator translator3 = new Translator(parts[i].substring(0, parts[i].length() - 1));
							String part3 = translator3.translate();
							ospitePhrase = String.join(" ", ospitePhrase, part3);
							;

							carattereSpeciale2 = parts[i].substring(parts[i].length() - 1, parts[i].length());

							ospitePhrase = String.join("", ospitePhrase, carattereSpeciale2);

						} else {

							Translator translator = new Translator(parts[i]);
							String part1 = translator.translate();

							ospitePhrase = String.join(" ", ospitePhrase, part1);

						}

					}

				} else {

					String[] partsWithTreat = parts[i].split("-");

					if (parts[i].equals(parts[0])) {

						if (endWithPunctuations(partsWithTreat[0])) {

							Translator translator3 = new Translator(
									partsWithTreat[i].substring(0, partsWithTreat[0].length() - 1));
							String part3 = translator3.translate();
							ospitePhrase = part3;

							carattereSpeciale2 = partsWithTreat[0].substring(partsWithTreat[0].length() - 1,
									partsWithTreat[0].length());

							ospitePhrase = String.join("", ospitePhrase, carattereSpeciale2);

						} else {

							Translator translator1 = new Translator(partsWithTreat[0]);
							String part2 = translator1.translate();

							ospitePhrase = part2;

						}

					} else {

						Translator translator1 = new Translator(partsWithTreat[0]);
						String part2 = translator1.translate();

						ospitePhrase = String.join(" ", ospitePhrase, part2);

					}

					for (int j = 1; j < partsWithTreat.length; j++) {

						if (endWithPunctuations(partsWithTreat[j])) {

							Translator translator3 = new Translator(partsWithTreat[j].substring(0, partsWithTreat[j].length() - 1));
							String part3 = translator3.translate();
							ospitePhrase = String.join("-", ospitePhrase, part3);

							carattereSpeciale2 = partsWithTreat[j].substring(partsWithTreat[j].length() - 1,
									partsWithTreat[j].length());

							ospitePhrase = String.join("", ospitePhrase, carattereSpeciale2);

						} else {

							Translator translator2 = new Translator(partsWithTreat[j]);
							String part3 = translator2.translate();

							ospitePhrase = String.join("-", ospitePhrase, part3);

						}

					}

				}

			}

			return ospitePhrase;

		}

		return NIL;

	}

	public String translate() {
		
		String punctuations = "";
		
		
		if (phrase.split(" ").length > 1 || phrase.split("-").length > 1) {

				return translateBaseSpace(phrase);

			}else if(endWithPunctuations(phrase) ){
				
			
				punctuations = phrase.substring(phrase.length()-1, phrase.length());
				
				
				Translator translator = new Translator(phrase.substring(0, phrase.length()-1));
				String part1 = translator.translate();
			
	
				return part1 + punctuations;
			
			
			}else {
				if (startWithVowel()) {

					if (phrase.endsWith("y")) {
						return phrase + "nay";
					} else if (endWithVowel()) {
						return phrase + "yay";
					} else if (!endWithVowel()) {
						return phrase + "ay";
					}

				} else if (phrase.length() > 0 && checkConsonant(phrase)) {

					int flag = 0;

					String newPhrase = "";

					newPhrase = phrase.substring(1, phrase.length()) + phrase.charAt(0);

					do {

						if (checkConsonant(newPhrase)) {

							newPhrase = newPhrase.substring(1, newPhrase.length()) + newPhrase.charAt(0);

						} else {

							flag = 1;

						}

					} while (flag == 0);

					return newPhrase + "ay";

				}

			}
		
		return NIL;
		
		}
	

		

	

	public boolean checkConsonant(String phraseInput) {

		return !(phraseInput.startsWith("a") || phraseInput.startsWith("e") || phraseInput.startsWith("i")|| phraseInput.startsWith("o") || phraseInput.startsWith("u"));

	}

	public boolean startWithVowel() {

		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o")|| phrase.startsWith("u");

	}

	public boolean endWithVowel() {

		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o")|| phrase.endsWith("u");

	}

	public boolean endWithPunctuations(String checkPhrase) {

		return checkPhrase.endsWith("?") || checkPhrase.endsWith("!") || checkPhrase.endsWith(",")|| checkPhrase.endsWith("(") || checkPhrase.endsWith(")") || checkPhrase.endsWith("'");

	}

}
