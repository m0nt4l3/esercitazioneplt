package esercitazioneplt;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TranslateTest{

	@Test
	void checkTranslator() {
		String inputPhrase = "hello word";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello word", translator.getPhrase());
	}

	@Test
	void checkPhraseTranslatorNull() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	void testTranslationPhraseStartinWithaAndFinishWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	void testTranslationPhraseStartinWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
  @Test
    void testTranslationPhraseStartinWithVowelEndingWithVovel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
  }
  
  @Test
  void testTranslationPhraseStartinWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
}
	
  @Test
  void testTranslationPhraseStartinWithOneConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
}
  
  @Test
  void testTranslationPhraseStartinWithMoreConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
}
  
  @Test
  void testTranslationPhraseWithSpace() {
		String inputPhrase = "hello word ";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay ordway", translator.translate());
}
  
  
  @Test
  void testTranslationPhraseWithTreat() {
		String inputPhrase = "hello-word";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay-ordway", translator.translate());
}
 
  @Test
  void testTranslationPhraseWithTreatAndWithSpace() {
		String inputPhrase = "hello word-hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay ordway-ellohay", translator.translate());
}
  @Test
  void testTranslationoneWordWithPunctuationsSpace() {
		String inputPhrase = "hello?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay?", translator.translate());
}
  
  @Test
  void testTranslationPhraseWithPunctuationsSpace() {
		String inputPhrase = "hello word?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay ordway?", translator.translate());
}
  
  @Test
  void testTranslationPhraseWithFistrWordWithPunctuationsSpace() {
		String inputPhrase = "hello? word?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay? ordway?", translator.translate());
}
 
  @Test
  void testTranslationPhraseWithPunctuationsTreat() {
		String inputPhrase = "hello-word?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay-ordway?", translator.translate());
}
  
  @Test
  void testTranslationPhraseWithFistrWordWithPunctuationsTreat() {
		String inputPhrase = "hello?-word?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay?-ordway?", translator.translate());
}
  
  
  @Test
  void testTranslationPhraseWithPunctuationsTreatandSpace() {
		String inputPhrase = "hello-word hello?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay-ordway ellohay?", translator.translate());
}
  
  @Test
  void testTranslationPhraseWithPunctuationsTreatandSpaceCase2() {
		String inputPhrase = "hello?-word hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay?-ordway ellohay", translator.translate());
}
  

  
  
  
}
  
  
